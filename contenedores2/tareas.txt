# nota : para todas estas actividades importante crear ruta en ejercicios/, ir realizando pantallazos 
# y dejandolos en la ruta de ejercicios. 
# ejemplo : si es /home/docker/pruebas/contenedores1 crear una ruta en /home/docker/ejercicios/contenedores1
# guardar los pantallazos en esa ruta y subirlo al repositorio con un git add -A, git commit -m "contenedores1", git push origin master

# ejecutar 'conpuertos'
# intentar conectarse al contenedor desde el host 
#hay que sacar la ip del host 

docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' db1

# con esa IP nos podemos conectar con un cliente sql desde el host, por el puerto que hayamos especificado 
# al levantar el contenedor (-p ..:...)
por ejemplo, si usamos -p 3307:3306 podemos, desde el host, hacer
telnet localhost 3307 y el trafico se enviara al puerto 3306 del contenedor

## se puede usar lo siguiente para exportar toda la bbdd 

docker exec db1 sh -c 'exec mysqldump --all-databases -uroot -p"$MYSQL_ROOT_PASSWORD"' > /tmp/all-databases.sql


# descargar DBeaver e instalarlo en la máquina virtual
# conectarse a la base de datos especificando como dirección IP localhost y el puerto 3307


# levantar otra instancia con un puerto distinto 
# tirar los contenedores y eliminarlos cuando se haya finalizado